function addemployees(){

    const nbEmp = parseInt(document.getElementById("numemployees").value);
    const employees = [];
    let employeeString = "";

    for (var i = 0 ; i < nbEmp ; i++){

        let employeeInput = prompt ("enter the employee details (username , email , role) : " );

        if(employeeInput == 0){
            alert(" Invalid input. Please enter your information. ");
        }

        const [username , email , role] = employeeInput.split(",");

        if (username == null || email == null || role == null){
            alert(" Invalid Information! Please enter your information");
            return;
        }
        
        employees.push({ username, email, role });
        
    }
    
    for (var i = 0 ;  i < employees.length ; i++){
       
        employeeString += "Username: " + employees[i].username + ", Email: " + employees[i].email + ", Role: " + employees[i].role + "<br>";

    }
    document.getElementById("demo").innerHTML = employeeString;

}
